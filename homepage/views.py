from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'Story3.html')

def hobby(request):
    return render(request, 'Story3page2.html')

def experience(request):
    return render(request, 'Story3page3.html')

def socmed(request):
    return render(request, 'Story3page4.html')