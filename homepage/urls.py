from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('hobby/', views.hobby, name='hobby'),
    path('experience/', views.experience, name='experience'),
    path('socmed/', views.socmed, name='socmed'),
]